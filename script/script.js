// 1. Екранування потрібне для того, щоб символи розпізнавались як текст, а не частина коду.
// наприклад можна поставити слеш 'Ім\'я' і тоді рядок зчитає правильно
// 2. Є оголошення фунції(декларація) i фунцкіональний вираз, коли функція записується в змінну
// 3. hoisting - це cпливання, змінні і функції переміщаються на початок своєї області видимості перед тим, як код буде виконано.

function createUser(){
    let firstName = prompt("Введіть ваше ім'я:");
    let lastName = prompt("Введіть ваше прізвище:");
    let birthday = prompt("Введіть вашу дату народження", 'dd.mm.yyyy');
    let bArr = birthday.split('.');
    let bDate = new Date(+bArr[2],bArr[1]-1,+bArr[0]);
    let today = new Date();
    
    return {
        name: firstName,
        lastname: lastName,
        bday: bDate,
        getLogin: function() {
            return this.name.charAt(0).toLowerCase() + this.lastname.toLowerCase();
        },
        getPassword: function() {
            return this.name.charAt(0).toUpperCase() + this.lastname.toLowerCase() + this.bday.getFullYear();

        },
        getAge: function() {
            return today.getFullYear() - this.bday.getFullYear();
        }
    };
}

let newUser = createUser();
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());
